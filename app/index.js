const express = require("express");
const app = express();
const PORT = 3000;
const api = require("./api");
const client = require('prom-client');
const collectDefaultMetrics = client.collectDefaultMetrics;
const getDockerContainerId = require('docker-container-id');
const Registry = client.Registry;
const register = new Registry();

getDockerContainerId().then(DOCKER_CONTAINER_ID => {
    collectDefaultMetrics({
        labels: { DOCKER_CONTAINER_ID },
        register
    });
})


app.get("/", (req, res) => {
    res.send(`
    <h3>Welcome to my Devops Project!</3>
    Made by <a href="https://alaajerbi.com">Alaa Jerbi</a>
    `)
})

app.get("/metrics", async (req, res) => {
    try {

        res.set('Content-Type', register.contentType);
        res.end(await register.metrics());

    } catch (ex) {
        console.log("error")
        res.status(500).send(ex);
    }
})

app.get("/fibonacci", (req, res) => {
    const { n } = req.query;

    if (n === undefined || n.length === 0 || isNaN(n)) {
        res.json({
            "status": "error",
            "error": "Query parameter n is missing/invalid"
        });
    }
    else {
        res.json({
            "status": "ok",
            "result": api.fibonacci(n)
        });
    }
});

app.get("/factorial", (req, res) => {
    const { n } = req.query;

    if (n === undefined || n.length === 0 || isNaN(n)) {
        res.json({
            "status": "error",
            "error": "Query parameter n is missing/invalid"
        });
    }
    else {
        res.json({
            "status": "ok",
            "result": api.factorial(n)
        });
    }
})

app.listen(PORT, () => {
    console.log(`Server listening on ${PORT}`);
})
